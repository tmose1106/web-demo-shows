from csv import DictReader
from datetime import date, datetime
from pathlib import Path

from bs4 import BeautifulSoup

my_data = DictReader(Path('./events.csv').open())

# print(my_data)

document = BeautifulSoup(features='html5lib')

events_container = document.new_tag('div', id='events')

for event in my_data:
    artists = event['artists'].split(',')
    date = datetime.strptime(event['date'], '%Y-%m-%d')
    title = event['title'] if event['title'] else '{} {} Tour'.format(artists[0], date.year)

    event_container = document.new_tag('div')
    event_container['class'] = 'event'

    event_title = document.new_tag('h3')
    event_title.string = title

    event_container.append(event_title)

    venue = document.new_tag('div')
    venue.string = '{1} at {0}'.format(event['venue'], date.strftime('%a, %b %d'))

    event_container.append(venue)

    image_container = document.new_tag('div')
    image_container['class'] = 'image-container'

    image_path = 'img/{}-{}.jpg'.format(
        event['date'],
        (event['title'] if event['title'] else artists[0]).lower().replace(' ', '-')
    )

    event_image = document.new_tag('img', src=image_path)

    image_container.append(event_image)

    event_container.append(image_container)

    artists_list = document.new_tag('ol')
    artists_list['class'] = 'event-artists'

    for artist in artists:
        artist_item = document.new_tag('li')
        artist_item.string = artist

        artists_list.append(artist_item)

    event_container.append(artists_list)

    events_container.append(event_container)

template = '''
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
  <link href="index.css" rel="stylesheet">
</head>
<body>
  <h1>My Concerts</h1>
  <p>
    A list of concerts I have been to over the years.
  </p>
  {0}
</body>
</html>
'''

output_file = Path('./index.html')

output_file.write_text(template.format(events_container.prettify()))